Je suis très heureuse de vous présenter l’atelier “Glace Graphique”.

Ce projet à été conçu par le collectif Figures Libres en Mai 2023 
( avec Maud Boyer, Sandrine Ripoll, Chloé Listrat, Zoé Berthelot (stagiaire))

Il a été mis en place le 14 mai à l’occasion de la 5e édition des Chantiers du 55 au 55 avenue Jean Jaurès à Arcueil. 

Il fut un véritable succès auprès des enfants comme des parents. 

Vous trouverez dans ce projet :

	01_references :
         la première étape du travail, la collecte de références graphiques ainsi que la référence de la peinture utilisée.
	02_dessin :
         les fichiers modifiables (svg) du projet :
            “patron” fichier regroupant les boules et les cornets 
            “dessin” composition de cornets de glaces
	03_patron : 
        un pdf regroupant les patrons (finaux) utilisés 
    04_devanture : 
        les fichiers qui ont servi a réalisé la devanture du stand pour le décorer
    05_photos : 
        et enfin les photos retouchés de l’atelier lors du 14 après-midi


Zoé Berthelot


